var updateBlockedCount = function (request, sender, sendResponse) {
  var total = request.count.toString();
  if(request.count >= 1000) {
    total = Math.round(request.count / 1000) + 'K+';
  }
  chrome.browserAction.setBadgeText({text: total});
};

chrome.runtime.onMessage.addListener(updateBlockedCount);

chrome.browserAction.setBadgeBackgroundColor({color: "#1da1f2"});
