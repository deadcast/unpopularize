var totalBlocked = 0;

var clean = function () {
  var tweets = document.querySelectorAll('.ProfileTweet-actionCount');
  
  tweets.forEach(function (el) {
    var likes = el.getAttribute('data-tweet-stat-count');
    if (likes > 1000) {
      el.closest('li.stream-item').remove();
      totalBlocked++;
    }
  });

  chrome.runtime.sendMessage({ count: totalBlocked });
};

var observer = new MutationObserver(function () {
  clean();
}).observe(document, { childList: true, subtree: true });

window.requestAnimationFrame(function() { clean(); })